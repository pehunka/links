Interesting links
=====

**Twitter Bootstrap plugins**

http://www.plugolabs.com/twitter-bootstrap-button-generator/

http://designshack.net/articles/css/20-awesome-resources-for-twitter-bootstrap-lovers/

http://mifsud.me/adding-dropdown-login-form-bootstraps-navbar/

http://taylor.fausak.me/2012/03/15/dropdown-menu-in-twitter-bootstraps-collapsed-navbar/

**ZURB Foundation**

http://www.zurb.com/playground

http://www.zurb.com/playground/foundation-icons

http://www.zurb.com/playground/responsive-tables

http://www.zurb.com/playground/off-canvas-layouts

**Interesting web**

http://patterntap.com/

http://demos.9lessons.info/

http://www.tdmarketing.co.nz/blog/2011/03/09/create-marker-with-custom-labels-in-google-maps-api-v3/

http://stackoverflow.com/questions/10808293/primefaces-how-to-redirect-to-mobile-version

http://css-tricks.com/resolution-specific-stylesheets/

http://niklasschlimm.blogspot.cz/2011/06/setting-up-spring-3-development.html#maven

http://www.alistapart.com/articles/responsive-web-design/

**JSF 2**

http://ovaraksin.blogspot.cz/2010/07/output-jsf-messages-with-static-and.html

**Useful Tools**

http://www.cssdrive.com/index.php/main/csscompressor/ (CSS COMPRESSOR)

http://www.colorzilla.com/gradient-editor/ (CSS GRADIENT EDITOR)

http://prefixmycss.com/ (CSS PREFIX)

http://www.mattkersley.com/responsive/  (RESPONSIVE DESIGN TESTING TOOL)

http://fontdragr.com/editor  (FONT TESTER)

http://www.generateprivacypolicy.com/ (PRIVACY POLICY GENERATOR)

http://loads.in/  (WEBPAGE LOADING SPEED TESTER)

http://browsershots.org/  (CROSS-BROWSER PREVIEWER)

http://ready.mobi (MOBILE-READY EVALUATOR)

**FREE ICONS KITs**

http://fortawesome.github.com/Font-Awesome/
http://iconmonstr.com/

**FREE FONTS KITs**

http://www.fontsquirrel.com/ (FREE FONTY)

**GLASSFISH **

http://leonardom.wordpress.com/2010/01/26/glassfish-v3-as-windows-service/